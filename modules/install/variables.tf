variable "namespace" {
  default = ""
}

variable "chart_version" {
  default = "3.1.0"
}

variable "extra_values" {
  default = {}
}
