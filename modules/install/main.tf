
locals {
  EXTRA_VALUES = {
  }
}


resource "helm_release" "release" {
  name       = "node-red"
  repository = "https://k8s-at-home.com/charts/"
  chart      = "node-red"
  version    = var.chart_version
  namespace  = var.namespace
  values = [
    yamlencode(local.EXTRA_VALUES),
    yamlencode(var.extra_values)
  ]
}
